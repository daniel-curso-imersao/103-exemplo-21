const deckEndpoint = "https://deckofcardsapi.com/api/deck/new/shuffle/";
const btnIniciar = document.querySelector("#iniciar");
const btnNovaCarta = document.querySelector("#carta");
const btnParar = document.querySelector("#parar");
const mesa = document.querySelector("#mesa");
let pontos = document.querySelector("#pontos");
const msg = document.querySelector("#msg");
let idBaralho;

function extrairJSON(resposta) {
    return resposta.json();
}

function preencherMesa(dados) {
    let cartas = dados.cards;
    for (let carta of cartas) {
        desenharCarta(carta);
        atualizarPlacar(carta.value);
    }    
}

function atualizarPlacar(valor) {
    if (valor === "ACE") {
        valor = 1;
    } else if (valor === "QUEEN" || valor === "KING" || valor === "JACK") {
        valor = 10;
    } else {
        valor = Number(valor);
    }    
    pontos.innerHTML = Number(pontos.innerHTML) + valor; 
    verificarPlacar();
}

function verificarPlacar() {
    let total = Number(pontos.innerHTML);
    if (total === 21) {
        msg.innerHTML = `Você ganhou o jogo com ${total}`;
        btnIniciar.onclick = iniciarJogo;
        btnNovaCarta.onclick = null;
        btnParar = null;
    } else if (total > 21) {
        msg.innerHTML = `Você perdeu o jogo com ${total}`;
        btnIniciar.onclick = iniciarJogo;
        btnNovaCarta.onclick = null;
        btnParar.onclick = null;
    }
}

function parar() {
    let total = Number(pontos.innerHTML);
    msg.innerHTML = `Você parou o jogo com ${total}`;
    btnNovaCarta.onclick = null;
    btnParar.onclick = null;
    btnIniciar.onclick = iniciarJogo;
}

function desenharCarta(carta) {
    let desenho = document.createElement("img");
    desenho.src = carta.image;
    desenho.alt = carta.code;
    mesa.appendChild(desenho);
}

function puxarCarta(qtde) {
    if (qtde != 2) qtde = 1;  
    fetch(`https://deckofcardsapi.com/api/deck/${idBaralho}/draw/?count=${qtde}`)
        .then(extrairJSON).then(preencherMesa);
}

function iniciarMesa(dados) {
    idBaralho = dados.deck_id;
    puxarCarta(2);
}

function obterBaralho() {
    fetch(deckEndpoint).then(extrairJSON).then(iniciarMesa);
}

function limpar() {
    mesa.innerHTML = "";
    msg.innerHTML = "Pontuação: <span id=\"pontos\">0</span>";
    pontos = document.querySelector("#pontos");
}

function iniciarJogo() {
    limpar();
    obterBaralho();
    btnIniciar.onclick = null;
    btnNovaCarta.onclick = puxarCarta;
    btnParar.onclick = parar;
}

btnIniciar.onclick = iniciarJogo;
btnParar.onclick = parar;